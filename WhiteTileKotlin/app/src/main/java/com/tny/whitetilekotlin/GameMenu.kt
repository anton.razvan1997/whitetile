package com.tny.whitetilekotlin

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class GameMenu : AppCompatActivity() {
    var highScore: Number = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_menu)

        val preferences: SharedPreferences =
            this.getSharedPreferences("myPreferences", Context.MODE_PRIVATE)
        this.highScore = preferences.getInt("HighScorekey", 0)

        setMenuText()
        createNewGameButton()

        getSupportActionBar()?.hide()
        setActivityBackgroundColor(Color.rgb(9, 69, 60))
    }


    private fun createNewGameButton() {

        var newGameBtn: Button = findViewById(R.id.newGameBtn)
        newGameBtn.setBackgroundColor(Color.WHITE)
        newGameBtn.setTextColor(Color.rgb(9, 69, 60))
        newGameBtn.setTextSize(20F)

        newGameBtn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }

    private fun setMenuText() {

        var logo: TextView = findViewById(R.id.logoText)
        logo.setTextColor(Color.WHITE)
        logo.setTextSize(50F)

        val menuHighScore: TextView = findViewById(R.id.menuHighScore)
        menuHighScore.setTextColor(Color.WHITE)
        menuHighScore.setTextSize(35F)
        menuHighScore.setText("Your HighScore:\n${this.highScore}")

    }


    fun setActivityBackgroundColor(color: Number) {
        val view: View = this.getWindow().getDecorView()
        view.setBackgroundColor(color.toInt())
    }


}