package com.tny.whitetilekotlin


import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaPlayer
import android.os.Handler
import android.os.Looper
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

object Game {
    private var score = 0
    private var highScore = 0
    private var gameSound: MediaPlayer? = null
    private var blackRow1: Number? = null
    private var blackRow2: Number? = null
    private var blackRow3: Number? = null
    private var blackRow4: Number? = null
    private var tiles: Array<ImageView>? = null
    private var end = false
    private var lastButOneRowCompleted = false
    private var lastRowCompleted = false

    private var h = Handler(Looper.getMainLooper())
    private var runnable: Runnable? = null
    private var firstClick = 0


    private var delay = 1100
    fun setupGame(context: Context, actionBar: ActionBar?, app: AppCompatActivity) {

        this.score = 0
        this.highScore = 0
        this.gameSound = null
        this.blackRow1 = null
        this.blackRow2 = null
        this.blackRow3 = null
        this.blackRow4 = null
        this.tiles = null

        end = false
        lastButOneRowCompleted = false
        lastRowCompleted = false
        h = Handler()
        runnable = null
        firstClick = 0
        delay = 1100

        this.gameSound = MediaPlayer.create(context, R.raw.furelise)

        this.setupActionBar(context, actionBar, app)

        this.tiles = createTilesArray(app)
        setActionForTiles(app, context)

        initGame()
        startGame(app, context)

    }


    private fun setActionForTiles(
        app: AppCompatActivity,
        context: Context
    ) {
        for (i in 9..12) {
            this.tiles!![i].setOnClickListener {
                this.tiles!![i].setBackgroundColor(Color.GREEN)
                if (blackRow3 != i - 8) {
                    this.tiles!![i].setBackgroundColor(Color.RED)

                    h.removeCallbacks(runnable)
                    endGame(app, context)
                    end = true
                } else {
                    score++
                    firstClick = 1
                    setScore(score, app)
                    lastButOneRowCompleted = true
                }
            }
        }

        for (i in 13..16) {
            this.tiles!![i].setOnClickListener {
                this.tiles!![i].setBackgroundColor(Color.GREEN)
                if (blackRow4 != i - 12) {
                    this.tiles!![i].setBackgroundColor(Color.RED)

                    h.removeCallbacks(runnable)
                    endGame(app, context)
                    end = true
                } else {
                    score++
                    firstClick = 1
                    setScore(score, app)
                    lastRowCompleted = true
                }
            }
        }

    }

    private fun setupActionBar(context: Context, actionBar: ActionBar?, app: AppCompatActivity) {
        this.highScore = getHighScore(context)

        actionBar?.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM)
        actionBar?.setDisplayShowCustomEnabled(true)
        actionBar?.setCustomView(R.layout.my_action_bar)

        actionBar?.setBackgroundDrawable(ColorDrawable(Color.rgb(9, 69, 60)))
        displayHighScore(app)

    }

    private fun displayHighScore(app: AppCompatActivity) {
        var txtHighScore: TextView = app.findViewById(R.id.acBarHighScore)
        txtHighScore.setText("High Score: $highScore")
    }

    private fun getHighScore(context: Context): Int {
        val sharedPreferences: SharedPreferences =
            context.getSharedPreferences("myPreferences", Context.MODE_PRIVATE)
        return sharedPreferences.getInt("HighScorekey", 0)
    }


    private fun createTilesArray(app: AppCompatActivity): Array<ImageView> {
        val tile1: ImageView = app.findViewById(R.id.tile1)
        val tile2: ImageView = app.findViewById(R.id.tile2)
        val tile3: ImageView = app.findViewById(R.id.tile3)
        val tile4: ImageView = app.findViewById(R.id.tile4)
        val tile5: ImageView = app.findViewById(R.id.tile5)
        val tile6: ImageView = app.findViewById(R.id.tile6)
        val tile7: ImageView = app.findViewById(R.id.tile7)
        val tile8: ImageView = app.findViewById(R.id.tile8)
        val tile9: ImageView = app.findViewById(R.id.tile9)
        val tile10: ImageView = app.findViewById(R.id.tile10)
        val tile11: ImageView = app.findViewById(R.id.tile11)
        val tile12: ImageView = app.findViewById(R.id.tile12)
        val tile13: ImageView = app.findViewById(R.id.tile13)
        val tile14: ImageView = app.findViewById(R.id.tile14)
        val tile15: ImageView = app.findViewById(R.id.tile15)
        val tile16: ImageView = app.findViewById(R.id.tile16)

        return arrayOf(
            tile1,
            tile1,
            tile2,
            tile3,
            tile4,
            tile5,
            tile6,
            tile7,
            tile8,
            tile9,
            tile10,
            tile11,
            tile12,
            tile13,
            tile14,
            tile15,
            tile16
        )
    }

    private fun endGame(app: AppCompatActivity, context: Context) {
        if (this.gameSound != null && this.gameSound!!.isPlaying())
            this.gameSound!!.stop()
        if (score > highScore) {
            setHighScore(score, app)
            displayHighScore(app)
        }
        var alert = AlertDialog.Builder(context)
        alert.setCancelable(false)

        alert.setTitle("Game ended!")
        alert.setMessage("You Lose! \n   Your score: $score")


        alert.setPositiveButton(
            "OK"
        ) { dialog, which ->
            // Finish activity
            app.finish()
        }

        val dialog = alert.show()
        dialog.setCanceledOnTouchOutside(false)

    }

    private fun setScore(currentScore: Number, app: AppCompatActivity) {
        var txtScore: TextView = app.findViewById(R.id.acBarScore)
        txtScore.setText("Score: $currentScore")
        if (score != 0 && score % 10 == 0) {
            delay -= (delay / 5)
        }
    }

    private fun setHighScore(currentScore: Number, app: AppCompatActivity) {
        highScore = currentScore as Int
        var prefs: SharedPreferences =
            app.getSharedPreferences("myPreferences", Context.MODE_PRIVATE)
        var editor = prefs.edit()
        editor.putInt("HighScorekey", currentScore)
        editor.commit()
    }

    private fun initGame() {
        makeAllWhite()
        lastRowCompleted = false
        lastButOneRowCompleted = false
        blackRow1 = (1..4).random() //nice approach

        blackRow2 = (1..4).random()
        while (blackRow1 == blackRow2)
            blackRow2 = (1..4).random()

        blackRow3 = (1..4).random()
        while (blackRow2 == blackRow3)
            blackRow3 = (1..4).random()

        blackRow4 = (1..4).random()
        while (blackRow3 == blackRow4)
            blackRow4 = (1..4).random()

        this.tiles!![blackRow1 as Int].setBackgroundColor(Color.BLACK)
        this.tiles!![blackRow2 as Int + 4].setBackgroundColor(Color.BLACK)
        this.tiles!![blackRow3 as Int + 8].setBackgroundColor(Color.BLACK)
        this.tiles!![blackRow4 as Int + 12].setBackgroundColor(Color.BLACK)

    }

    private fun makeAllWhite() {
        for (tile in this.tiles!!) {
            tile.setImageResource(R.drawable.border)
            tile.setBackgroundColor(Color.WHITE)
        }

    }

    private fun startGame(app: AppCompatActivity, context: Context) {
        this.runnable = Runnable {
            h.postDelayed(runnable, delay.toLong())

            if (end) {
                h.removeCallbacks(runnable)
                if (this.gameSound!!.isPlaying())
                    this.gameSound!!.stop()

                endGame(app, context)
            } else {
                if (firstClick != 0) {
                    moveTiles(app, context)
                    if (!this.gameSound!!.isPlaying()){
                        this.gameSound!!.start()
                    }
                }
            }
        }

        h.postDelayed(runnable, delay.toLong())
    }

    private fun moveTiles(app: AppCompatActivity, context: Context) {
        if (!lastRowCompleted) {
            end = true
            h.removeCallbacks(runnable)
            endGame(app, context)
        } else {

            this.makeAllWhite()

            this.tiles!![blackRow1 as Int + 4].setBackgroundColor(Color.BLACK)
            this.tiles!![blackRow2 as Int + 8].setBackgroundColor(Color.BLACK)
            this.tiles!![blackRow3 as Int + 12].setBackgroundColor(Color.BLACK)

            blackRow4 = blackRow3
            blackRow3 = blackRow2
            blackRow2 = blackRow1

            blackRow1 = (1..4).random()
            while (blackRow1 == blackRow2)
                blackRow1 = (1..4).random()

            this.tiles!![blackRow1 as Int].setBackgroundColor(Color.BLACK)
            if (lastButOneRowCompleted) {
                this.tiles!![blackRow4 as Int + 12].setBackgroundColor(Color.GREEN)
            }

            lastRowCompleted = lastButOneRowCompleted
            lastButOneRowCompleted = false
        }

    }
}